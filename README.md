# Serverless Signaling
## Introduction

**WIP: Do not use this in any production like setting.**

This repository contains a JS programm to conduct WebRTC signaling without a signaling server. The programm uses the distributed hash table (DHT) Kademlia to establish an overlay network. The connections within this network are realized by using WebRTC's DataChannel. Signaling is conducted by routing Kademlia queries thourgh the DHT.

The underlying DHT is realized by using [webtorrent-dht](https://github.com/nazar-pc/webtorrent-dht "webtorrent-dht's repo").

## Documentation

A documentation of the API, which is created with JSDoc, can be found [here](https://derpsteb.gitlab.io/bitsig/WebtorrentDhtCustom.html "API Doc").

Possibly unknown abreviations:  
CIO - Connection Information Object.

## Installation

`npm install bitsig`

## Usage

To use the programm install it on your machine. Once installed, you can include the prebuilt **browser.js** file in every page that needs to access
the signaling service. Alternatively you can use node's require method and run your own build process. In this case your build process needs to include browserify.  
Require the package like this:  
```javascript
const BITSIG = require("bitsig");
const CLIENT_OPTIONS = {...}

let bitSig = new BITSIG(CLIENT_OPTIONS);
```

`CLIENT_OPTIONS` configures the underlying DHT.

The basic options are inherited by [bittorrent-dht](https://github.com/webtorrent/bittorrent-dht#api). Additionally there are four more options added by [webtorrent-dht](https://github.com/nazar-pc/webtorrent-dht#how-to-use).  
BitSig adds one more option:

* `keyPair`: A asymmetric [CryptoKey](https://developer.mozilla.org/en-US/docs/Web/API/CryptoKey) keypair. Looks like this:  
`{ privateKey: CryptoKey, publicKey: CryptoKey }`.  
Both keys need to be extractable and allowed to encrypt and decrypt (See linked docs.).  
A minimal running example can be found in the demoPage folder.

## Demo

To run the demo one first has to start the bootstrapping server. This is done by running: `node server.js`  
To enable debugging messages run: `DEBUG=webtorrent-dht node server.js`  
Once the bs server runs the **demoPage.html** file from the **demoPage** folder has to be served by a local webserver. Upon access the DHT object is initiated and bootstrapped.  
By accessing the same site in a second tab, a network with two nodes can be simulated. 

The demo page tries to show which have to be taken in which order.

1. Input an imaginary contentID into the field labled *Deck ID:* and press the **Create** button. This creates a new CIO 
and saves it to the DHT.
2. Copy the hash printed to the node status field. 
3. Open the same page again in a new tab. Paste the hash into the text field next to *Connect to Session* and press the **Connect** button.
This will retrieve the CIO and establish the described connection. A IP address should appear in the node status field.
4. Press the **Show** button in order to display the current application connections.
5. Press the **Activate** button to activate a video/audio stream.
6. Copy the targetID displayed by pressing the **Show** button into the text field next to *Add Stream* and press the **Add** button.
This will renegotiate the application connection and display the stream on the first tab, if successful.

## Build

To ship the client side code `browserify` is used. `npm run build` needs to be run within the repos root folder, if **demo.js** is changed.  
The same applies to **index.js**. **browser.js** is a prebuilt file ready to include in any html file.

## Contribution

Any feedback or contribution is highly appreciated.

## Licence

BitSig. A programm to conduct WebRTC signaling through a Kademlia based DHT.
Copyright (C) 2018 Otto Bittner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
