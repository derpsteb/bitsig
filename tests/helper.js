"use strict";
const DHT = require("../main");
module.exports = {
	nodeCreator: nodeCreator,
	bootstrapCreator: bootstrapCreator
};

function nodeCreator(i, t, CLIENT_OPTIONS) {
	let tmpDht = undefined;
	let p1 = new Promise((resolve) => {
		tmpDht = new DHT(CLIENT_OPTIONS);
		t.comment("Started Node " + i);
		tmpDht.once("failed", () => {
			t.fail("failed event");
		});
		tmpDht.once("DHT ready", () => {
			t.pass("DHT ready event received");
			//Check that there are actual connections
			let peer_connections = tmpDht._rpc.socket.socket._peer_connections;
			t.notEqual(Object.keys(peer_connections).length, 0, "Check for nr of connected nodes on Node " + i);
			t.equal(peer_connections.constructor, Object, "Check if _peer_connections is actually an object");
			resolve();
		});
	});
	return {"promise": p1, "node": tmpDht};
}

function bootstrapCreator(t, SERVER_OPTIONS){
	let bsDht = undefined;
	let p1 = new Promise((resolve) => {
		bsDht = new DHT(SERVER_OPTIONS);
		bsDht.listen(16882, "127.0.0.1", () => {});
		bsDht.once("listening", () => {
			t.pass("BS node running");
			resolve();
		});
	});
	return {"promise": p1, "node": bsDht};
}