"use strict";
const TAPE = require("tape");
const DHT = require("../main");
const BUFFER = require("buffer").Buffer;
const HELPER = require("./helper");

const SERVER_OPTIONS = {
	nodes: [],
	simple_peer_opts: {
		config: {
			iceServers: []
		}
	},
	isServer: true
};
const CLIENT_OPTIONS = {
	nodes: [{
		host: "127.0.0.1",
		port: 16882
	}],
	simple_peer_opts: {
		config: {
			iceServers: []
		}
	},
	isServer: false
};
//This array is used to delete all created dhts so the tests exits properly.
let running = [];


TAPE("bootstrapping tests", (t) => {
	const NODES = 10;
	const PAYLOAD = "1234Test1234";
	t.plan(NODES*3+4);

	let promises = [];

	// tmp := {node: node, promise: promise}
	let tmp = HELPER.bootstrapCreator(t, SERVER_OPTIONS);
	running.push(tmp.node);

	tmp.promise.then(() => {
		for(let i = 0; i < NODES; i++){
			let tmp = HELPER.nodeCreator(i, t, CLIENT_OPTIONS);
			running.push(tmp.node);
			promises.push(tmp.promise);
		}
		return Promise.all(promises);


	}).then(() => {
		//save string to random location
		return new Promise((resolve) => {
			let rndInt = Math.floor(Math.random() * Math.floor(NODES));
			t.comment("Node index calling put: " + rndInt);
			running[rndInt].put({v: BUFFER.from(PAYLOAD)}, (err, hash) => {
				t.notEqual(hash.toString("hex"), null, "Hash should be a non empty string");
				t.equal(err, null, "There shoud be no errors");
				resolve(hash);
			});
		});
	}).then((hash) => {
		//read string from random location
		return new Promise((resolve) => {
			let rndInt = Math.floor(Math.random() * Math.floor(NODES));
			t.comment("Node index calling get: " + rndInt);
			running[rndInt].get(hash, (err, res) => {
				t.deepEqual(res.v.toString(), PAYLOAD);
				resolve();
			});
		});
	}).then(() => {
		running.forEach((entry) => {
			entry.destroy();
		});
	});
});
