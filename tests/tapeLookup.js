"use strict";
const TAPE = require("tape");
const DHT = require("webtorrent-dht");
const HELPER = require("./helper");

const SERVER_OPTIONS = {
	nodes: [],
	simple_peer_opts: {
		config: {
			iceServers: []
		}
	},
	isServer: true
};
const CLIENT_OPTIONS = {
	nodes: [{
		host: "127.0.0.1",
		port: 16882
	}],
	simple_peer_opts: {
		config: {
			iceServers: []
		}
	},
	isServer: false
};
//This array is used to delete all created dhts so the tests exits properly.
let running = [];

TAPE("lookup test /w all nodes", (t) => {
	const NODES = 3;
	t.plan(NODES*3+1+(NODES+1)*(NODES+1));

	// tmp := {node: node, promise: promise}
	let tmp = HELPER.bootstrapCreator(t, SERVER_OPTIONS);
	running.push(tmp.node);

	let promises = [];
	tmp.promise.then(() => {
		for(let i = 0; i < NODES; i++){
			tmp = HELPER.nodeCreator(i, t, CLIENT_OPTIONS);
			running.push(tmp.node);
			promises.push(tmp.promise);
		}
		return Promise.all(promises);
	}).then(() => {
		//lookup every other node for every node
		let lookupPromises = [];
		for(let i = 0; i < running.length; i++){
			for(let j = 0; j < running.length; j++){
				//looking peer
				let seeker = running[i];
				let seeked = running[j];
				let p2 = new Promise((resolve, reject) => {
					seeker.lookup(seeked.nodeId.toString("hex"), (err, foundNodes) => {
						if(err === null){
							t.comment("Found node " + j + " from Node " + i);
							t.pass();
							resolve();
						} else {
							console.log("Error in " + i + " " + j + ": " + err);
							reject();
						}
					});
				});
				lookupPromises.push(p2);
			}
		}
		return Promise.all(lookupPromises);
	}).then(() => {
		running.forEach((entry) => {
			entry.destroy();
		});
	});
});