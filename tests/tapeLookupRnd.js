"use strict";
const TAPE = require("tape");
const HELPER = require("./helper");

const SERVER_OPTIONS = {
	nodes: [],
	simple_peer_opts: {
		config: {
			iceServers: []
		}
	},
	isServer: true
};
const CLIENT_OPTIONS = {
	nodes: [{
		host: "127.0.0.1",
		port: 16882
	}],
	simple_peer_opts: {
		config: {
			iceServers: []
		}
	},
	isServer: false
};
//This array is used to delete all created dhts so the tests exits properly.
let running = [];

TAPE("lookup test /w rnd nodes", (t) => {
	const NODES = 15;
	t.plan(NODES*3+2);

	let tmp = HELPER.bootstrapCreator(t, SERVER_OPTIONS);
	running.push(tmp.node);

	let promises = [];
	tmp.promise.then(() => {
		for(let i = 0; i < NODES; i++){
			tmp = HELPER.nodeCreator(i, t, CLIENT_OPTIONS);
			running.push(tmp.node);
			promises.push(tmp.promise);
		}
		return Promise.all(promises);
	}).then(() => {
		let i = Math.floor(Math.random()*Math.floor(NODES));
		let seeker = running[i];
		let j = Math.floor(Math.random()*Math.floor(NODES));
		let seeked = running[j];
		return new Promise((resolve, reject) => {
			seeker.lookup(seeked.nodeId.toString("hex"), (err, foundNodes) => {
				if(err === null){
					t.comment("Found node " + j + " from Node " + i);
					t.pass();
					resolve();
				} else {
					console.log("Error in " + i + " " + j + ": " + err);
					reject();
				}
			});
		});
	}).then(() => {
		running.forEach((entry) => {
			entry.destroy();
		});
	});
});