"use strict";

(function () {
	const DHT = require("./main");
	const options = {
		nodes: [],
		simple_peer_opts: {
			config: {
				iceServers: []
			}
		},
		isServer: true
	};
	global.buffer = require("buffer").Buffer;
	global.bencode = require("bencode");
	global.dht = new DHT(options);

	global.dht.listen(16881, "127.0.0.1", function () {
		console.log("now listening");
		console.log(JSON.stringify(global.dht.toJSON()));
	});
	global.dht.on("error", function (err) {
		console.log(err);
	});
	global.dht.on("warning", function (war) {
		console.log(war);
	});

}).call(this);
