// Do not pay attention to this file. Playground to test code.
"use strict";

const webtorrentDht = require("./main");
global.magnet = require("magnet-uri");
global.buffer = require("buffer").Buffer;
global.bencode = require("bencode");
global.startUp = startUp;
global.test1 = test1;
global.test2 = test2;
//startUp();

global.retrieve = retrieve;
global.save = save;
global.showConnectedIds = showConnectedIds;
global.showId = showId;
global.lookup = lookup;

function startUp () {
	let options = {
		bootstrap: [{
			host: "127.0.0.1",
			port: 16881
		}],
		simple_peer_opts: {
			config: {
				iceServers: []
			}
		},
		isServer: false
	};
	global.dht = new webtorrentDht(options);
	// localStorage.debug = "webtorrent-dht";
}

function test1() {
	let tmp;
	let options = {
		nodes: [{
			host: "127.0.0.1",
			port: 16881
		}],
		simple_peer_opts: {
			config: {
				iceServers: []
			}
		},
		isServer: false
	};
	let chain = Promise.resolve();
	for(let i = 0; i < 2; i++){
		chain = chain.then(newDHT()).then(wait());
	}
	//tmp = newDHT().then(() => newDHT()).then(() => newDHT());

	function wait(){
		return new Promise((resolve, _reject) => { setTimeout(resolve, 3000); });
	}

	function newDHT(){
		return new Promise((resolve, _reject) => {
			tmp = new webtorrentDht(options);
			Object.getPrototypeOf(tmp).on("ready", () => {
				console.log("promise resolved");
				resolve();
			});
		});
	}
}

function test2(){
	let options = {
		nodes: [{
			host: "127.0.0.1",
			port: 16881
		}],
		simple_peer_opts: {
			config: {
				iceServers: []
			}
		},
		isServer: false
	};
	let dht1 = new webtorrentDht(options);
	Object.getPrototypeOf(dht1).on("node", () => {
		console.log("dht1 rdy");
	});
	let dht2 = new webtorrentDht(options);
	Object.getPrototypeOf(dht2).on("node", () => {
		console.log("dht2 rdy");
	});
	let dht3 = new webtorrentDht(options);
	Object.getPrototypeOf(dht3).on("node", () => {
		console.log("dht3 rdy");
	});
	dht3.lookup(dht1.nodeId.toString("hex"), (err, res) => {
		console.log("error: " + err);
		console.log("res: " + res);
	});
}

function save (msgString) {
	let value = this.buffer.from(msgString);
	this.dht.put({v: value}, (err, hash) => {
		hash = hash.toString("hex");
		createLink(hash);
		console.log("Hash " + hash);
		console.log("Error " + err);
	});
}

function retrieve (link) {
	let hash = this.magnet.decode(link).infoHash;
	this.dht.get(hash, (err, res) => {
		console.log("errors: " + err);
		if (res === null) {
			console.log("No Data found.");
		} else {
			console.log("Retrieved value: " + this.buffer.from(res.v).toString());
		}
	});
}

function createLink (hash) {
	let uri = global.magnet.encode({
		infoHash: [hash]
	});
	let list = global.document.getElementById("magnets");
	let entry = global.document.createElement("li");
	entry.appendChild(global.document.createTextNode(uri));
	list.appendChild(entry);
}

function showConnectedIds () {
	let dict = global.dht._rpc.socket.socket._peer_connections;
	for (var key in dict) {
		console.log(dict[key]["id"]);
	}
}

function showId(){
	console.log(global.buffer.from(global.dht.nodeId).toString('hex'));
}

function lookup(id){
	global.dht.lookup(id, (err, foundNode) => {
		console.log("error: " + err);
		console.log("found nodes: " + foundNode);
	})
}