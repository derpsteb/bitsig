// BitSig. A program to conduct WebRTC signaling through a Kademlia based DHT.
// Copyright (C) 2018 Otto Bittner
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// 	This program is distributed in the hope that it will be useful,
// 	but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
// 	You should have received a copy of the GNU General Public License
//  along with this program.  If not, see http://www.gnu.org/licenses/.

"use strict";

module.exports = WebtorrentDhtCustom;
const Dht = require("webtorrent-dht");
const Bencode = require("bencode");
const Buffer = require("buffer").Buffer;
const inherits = require("inherits");
const SimplePeer = require("simple-peer");
const wrtc = require("wrtc");
const WebCrypto = require("node-webcrypto-ossl");
const TextEncoder = require("text-encoding").TextEncoder;
const TextDecoder = require("text-encoding").TextDecoder;
let isNode = require("detect-node");

//Check the running environment and load module if node is detected
if(isNode){
	global.crypto = new WebCrypto();
}

/**
 * Extension of the webtorrent-dht base class. Main class of the software. Acts as interface to access the underlying DHT
 * and trigger signaling.
 * @param {Object} options webtorrent-dht-options: values, <br> keyPair: WebCrypto key pair; defaults to 2048 bit RSA keypair,
 * <br> streamHandler: function to handle incoming streams; defaults to empty function; expects stream object, <br>
 * trackHandler: function to handle incoming tracks; defaults to empty function; expects track and associated stream
 * @returns {WebtorrentDhtCustom}
 * @constructor
 * @constructs WebtorrentDhtCustom
 */
function WebtorrentDhtCustom(options) {
	// targetId : simplePeer
	this.peerConnections = {};

	// Functions to handle incoming streams and tracks.
	this.streamHandler = options.streamHandler;
	this.trackHandler = options.trackHandler;

	//TODO Check what this variable is used for.
	//this.isServer = options.isServer;

	//Hosted Sessions - Array of IDs, One ID per hosted content
	this.runningSessions = [];

	// nodeID : AES key - Session key for every associated node.
	this.sessionKeys = {};

	this.keyPair = undefined;
	if(options.keyPair === undefined){
		global.crypto.subtle.generateKey({
			name: "RSA-OAEP",
			modulusLength: 2048,
			publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
			hash: {name: "SHA-256"}}, true, ["encrypt", "decrypt"]).then((keyPair) => {
			this.keyPair = keyPair;
		});
	} else {
		this.keyPair = options.keyPair;
	}



	if (!(this instanceof WebtorrentDhtCustom)) {
		return new WebtorrentDhtCustom(options);
	}
	Dht.call(this, options);
	this.on("ready", () => {
		this.announce(this.nodeId.toString("hex"), (error) => {
			//Used for proper tape tests.
			if(error !== null){
				this.emit("failed");
			}
			this.emit("DHT ready");
			// console.log("DHT ready");
		});
	});
}

inherits(WebtorrentDhtCustom, Dht);

/**
 * Retrieves a CIO from the DHT.
 * @param {String} infoHash hash (default: SHA-1) of the CIO that is to be retrieved.
 * @returns {Promise} Promise which resolves to CIO.
 */
WebtorrentDhtCustom.prototype.retrieveCio = function (infoHash) {
	let self = this;
	//Processing is done by assigning a handler to the returned promise which gets the ConnInfo
	return this._getCio(infoHash).then((res) => {
		if(res === null){
			//TODO An event or sth similiar should be emitted here.
			console.log("No value found for this key");
		} else {
			return JSON.parse(res.v.toString());
		}
	}, (err) => {
		console.error("Error in retrieveCio: " + err);
		console.trace();
	});
};

/**
 * Wrapper for webtorrent-dht.get method to allow promise based syntax.
 * @param infoHash SHA-1 hash of the to be retrieved object.
 * @returns {Promise} Promise which resolves to the desired object.
 * @private
 */
WebtorrentDhtCustom.prototype._getCio = function (infoHash) {
	let self = this;
	return new Promise(function(resolve, reject) {
		self.get(infoHash, (err, res) => {
			if(err){
				reject(err);
			} else {
				resolve(res);
			}
		});
	});
};

/**
 * Create a CIO for the specified content the session shall provide. <br>
 * Publishes the created CIO to the DHT.
 * @param {String} id of the provided content.
 * @returns {Promise} Resolves to the hash (default: SHA-1) of the created CIO.
 */
WebtorrentDhtCustom.prototype.cioCreator = function(contentId){
	let self = this;
	let ConnInfo = this._createCio(contentId);
	return ConnInfo.then((info) => {
		return self._publishCio(info);
	}).then(
		(hash) => {
			console.log("Successfully saved ConnInfo: " + hash.toString("hex"));
			return new Promise(function(resolve) {
				self.announce(hash, () => {
					console.log("Successfully announced ConnInfo with hash: " + hash.toString("hex"));
					resolve(hash.toString("hex"));
				});
			});
		},
		(err) => {
			console.error("Error occured in createRoom: " + err);
			console.trace();
		}
	);
};

/**
 * This could be tweaked to make ConnInfo configurable. <br>
 * Exports the public key of this node to a jwk format and writes it into the ConnInfo object.
 * @param {String} contentId ID of the provided content.
 * @returns {{contentId: String, initiator: String, pubKey: String}} The ConnInfo object.
 * @private
 */
WebtorrentDhtCustom.prototype._createCio = function(contentId){
	//Export pubKey so it can be sent
	return global.crypto.subtle.exportKey("jwk", this.keyPair.publicKey).then((keyText) => {
		//RoomID should be managed by central server - for now its just a value put in by the user
		let ConnInfo = {
			contentId: contentId,
			initiator: this.nodeId.toString("hex"),
			pubKey: JSON.stringify(keyText)
		};
		//Remember to parse when retrieving
		return ConnInfo;
	});
};

/**
 * Saves ConnInfo to DHT.
 * @param {Object} ConnInfo Object containing necessary info. See _createCio for structure.
 * @returns {Promise} Promise which resolves to SHA-1 hash of the created ConnInfo.
 * @private
 */
WebtorrentDhtCustom.prototype._publishCio = function(ConnInfo){
	let self = this;
	const value = Buffer.from(JSON.stringify(ConnInfo));
	return new Promise(function(resolve, reject) {
		self.put({v: value}, (err, hash) => {
			if (err) {
				reject(err);
			} else {
				resolve(hash);
			}
		});
	});
};

/**
 * Creates a WebRTC connection to the host of the given CIO. <br>
 * Saves the connection to this.peerConnections.
 * @param {Object} cio Information about the session's host and its public key.
 * @param {Object} peerConfig Settings object for the simple-peer object created. All configuration of the established
 * connection is done through this object. Refer to simple-peers documentation for more info. Defaults to a simple data channel.
 * @returns {Promise} Resolves to peer object representing local end of new connection.
 */
WebtorrentDhtCustom.prototype.initConnection = function(cio, peerConfig = {initiator: true, trickle: false}) {
	let self = this;
	let peer = new SimplePeer(peerConfig);
	this.peerConnections[cio.initiator] = peer;

	return new Promise((resolve, reject) => {
		peer.on("signal", (signal) => {
			//send data to cio.initiator as connection query
			const msg = {y: "q", q: "connection", a: { targetId: cio.initiator, originId: this.nodeId.toString("hex"), signal: JSON.stringify(signal) }};

			let promise = undefined;
			if(self.sessionKeys[cio.initiator] === undefined){
				promise = self._initAESSession(cio);
			} else {
				promise = Promise.resolve();
			}
			promise.then(() => {
				self.sendAESEncryptedMsg(cio.initiator, msg);
			}).then((newConn) => {
				self.newConn = newConn;
			});

			peer.on("connect", () => {
				//Even though this is not async it is ok in this case since a legitimate "connect" event can only be fired
				//after a message has been sent successfully. Also: If this property is undefined and the test fails no harm is done.
				//The connection will just continue to exist as duplicate.
				if(self.newConn) {
					self._removeConnection(cio.initiator);
				} else {
					console.log("No new connection, so no closing of connection");
				}
				resolve(peer);
			});

			peer.on("data", (data) => {
				console.log("New data from remote: " + data);
			});

			peer.on("close", () => {
				console.log("connection closed");
			});

			peer.on("error", (err) => {
				reject(err);
			});
		});
	});
};

/**
 * Removes the connection to a node by removing all local ends of the connection and signaling the remote end that
 * it should do the same. Used to close duplicate connections created for signaling.
 * @param {String} targetId nodeId of the remote end.
 * @private
 */
WebtorrentDhtCustom.prototype._removeConnection = function(targetId){
	const targetBuffer = Buffer.from(targetId);
	this.nodes.remove(targetBuffer);
	this._rpc.nodes.remove(targetBuffer);
	const targetPeer = this._rpc.socket.socket.get_id_mapping(targetId);
	const tmp = targetPeer.remoteAddress + ":" + targetPeer.remotePort;
	this._rpc.socket.socket._peer_connections[tmp].destroy(() => {
		console.log("Negotiation channel closed");
	});
	// delete this._rpc.socket.socket.connections_id_mapping[targetId];
	// delete this._rpc.socket.socket._peer_connections[tmp];
};


/**
 * Creates, exports and sends an AES key to the given session host.
 * @param {Object} ConnInfo Information about the session host and its public key.
 * @returns {Promise} Promise which resolves to a boolean indicating if a newly created connection has been used to
 * send the AES key. This is then used to decide if the connection needs to be closed again.
 * @private
 */
WebtorrentDhtCustom.prototype._initAESSession = function(ConnInfo){
	let self = this;
	let alg = {name: "AES-GCM", length: 256};

	return global.crypto.subtle.generateKey(alg, true, ["encrypt", "decrypt"]).then((key) => {
		self.sessionKeys[ConnInfo.initiator] = key;
		return global.crypto.subtle.exportKey("jwk", key);
	}).then((exportedKey) => {
		let msg = {key: exportedKey};
		return self.sendRSAEncryptedMsg(ConnInfo.initiator, msg, JSON.parse(ConnInfo.pubKey));
	});
};

/**
 * Sends a message encrypted with AES operating in GCM mode.
 * @param {String} targetId The nodeID of the recipient.
 * @param {Object} msg The message object to be sent.
 * @returns {Promise} Resolves to a boolean indicating if a newly created connection has been used to
 * send the AES key. This is then used to decide if the connection needs to be closed again (done automatically).
 */
WebtorrentDhtCustom.prototype.sendAESEncryptedMsg = function(targetId, msg){
	let self = this;
	let iv = global.crypto.getRandomValues(new Uint8Array(16));

	const alg = {name: "AES-GCM", iv: iv};
	const key = this.sessionKeys[targetId];
	const data = new TextEncoder().encode(JSON.stringify(msg));

	return global.crypto.subtle.encrypt(alg, key, data).then((encryptedMsg) => {
		return self.sendMsg(targetId, {y: "q", q: "encrypted_msg", a: {msg: new Uint8Array(encryptedMsg), iv: alg.iv}});
	});


};

/**
 * Sends a message encrypted with RSA operating in OAEP mode.
 * @param {String} targetId The nodeId of the recipient.
 * @param {Object} msg The message object to be sent.
 * @param {Object} publicKey CryptoKey instance. Defaults to the public key of the object.
 * @returns {Promise} Resolves to a boolean indicating if a newly created connection has been used to
 * send the AES key. This is then used to decide if the connection needs to be closed again.
 */
WebtorrentDhtCustom.prototype.sendRSAEncryptedMsg = function(targetId, msg, publicKey=this.keyPair.publicKey){
	let self = this;

	let alg = {name: "RSA-OAEP", hash: {name: "SHA-256"}};
	let data = new TextEncoder().encode(JSON.stringify(msg));

	let promise = undefined;
	if(publicKey === self.keyPair.publicKey){
		promise = new Promise((resolve) => {
			resolve(publicKey);
		});
	} else {
		promise = global.crypto.subtle.importKey("jwk", publicKey, alg, false, ["encrypt"]);
	}

	return promise.then((importedKey) => {
		return global.crypto.subtle.encrypt({name: "RSA-OAEP"}, importedKey, data).then((encryptedMsg) => {
			//Send encrypted MSG here.
			return self.sendMsg(targetId, {y: "q", q: "encrypted_msg", a: new Uint8Array(encryptedMsg)} );
		}).catch(function(error) {
			console.error(error);
		});
	});
};

//TODO This method should only be used, after an AESsession is created with the communications partner.
/**
 * Provides a direct WebRTC connection to the node targetId and sends a message to it.
 * If the connection is not established yet it is established by calling dht.lookup() with the given targetId.
 * @param {String} targetId The nodeId of the recipient.
 * @param {String} msg The message object to be sent.
 * @returns {Promise} Resolves to a boolean indicating if a newly created connection has been used to
 * send the message. This is then used to decide if the connection needs to be closed again (done automatically).
 */
WebtorrentDhtCustom.prototype.sendMsg = function(targetId, msg){
	let self = this;
	let targetNode = self._rpc.socket.socket.get_id_mapping(targetId);
	const newConn = !targetNode;
	let promise = undefined;
	console.log(msg);

	if(targetNode !== undefined){
		promise = new Promise(function(resolve) {
			resolve(targetNode);
		});
	} else {
		promise = self._lfTarget(targetNode, targetId);
	}
	return promise.then((targetNode) => {
		targetNode.send(Bencode.encode(msg));
		console.log("sendingmsg");
		return newConn;
	});
};

/**
 * Searches the DHT for the node with id the given id.
 * @param targetId nodeId of the node to be found.
 * @returns {Promise} Promise which resolves to the peer object representing the connection.
 * @private
 */
WebtorrentDhtCustom.prototype._lfTarget = function(targetId){
	//Lookup the targetId
	let self = this;
	let targetAddress = undefined;
	this.on("peer", (peer, _infoHash, _from) => {
		targetAddress = peer.host + ":" + peer.port;
	});
	return new Promise(function(resolve, reject) {
		//Check if there is already a direct connection
		console.log("calling lookup");
		self.lookup(targetId, (err, foundNodes) => {
			if (err === null) {
				resolve(foundNodes);
			} else {
				reject(err);
			}
		});
	}).then((foundNodes) => {
		console.log("Lookup success: " + foundNodes);
		console.log("targetAddress: " + targetAddress);
		//If targetAddress got assigned there was a successfull lookup and targetNode had to be found first
		return self._rpc.socket.socket._peer_connections[targetAddress];
	}).catch((err) => {
		console.log("Lookup failed me: "+ err);
		return null;
	});
};

/**
 * Default onquery method of bittorrent-dht extended by a custom query type "connection" to realizes connection
 * establishment through the dht.
 * @param query Message object with which the query event is emitted
 * @param peer Remote peer sending the query
 * @returns {*}
 * @private
 */
WebtorrentDhtCustom.prototype._onquery = function(query, peer) {
	// console.log("received query in DHT.customOnQuery: ");
	let q = query.q.toString();
	this._debug("received %s query from %s:%d", q, peer.address, peer.port);
	if (!query.a) return;

	switch (q) {
	case "ping":
		return this._rpc.response(peer, query, {id: this._rpc.id});

	case "find_node":
		return this._onfindnode(query, peer);

	case "get_peers":
		return this._ongetpeers(query, peer);

	case "announce_peer":
		return this._onannouncepeer(query, peer);

	case "get":
		return this._onget(query, peer);

	case "put":
		return this._onput(query, peer);

	case "connection":
		return this._onconnection(query, peer);

	case "general_message":
		return this._onMessage(query, peer);

	case "encrypted_msg":
		return this._onEncryptedMsg(query, peer);
	}
};

/**
 * React to incoming signaling data.
 * Check if a simple-peer for the remote peer was already created, if not do so.
 * Calls signal() on peer object.
 * @param peer simple-peer object representing the to be established connection
 * @param query message object holding signal data, originId and targetId.
 * @private
 */
WebtorrentDhtCustom.prototype._processConnection = function(query, peer){
	let self = this;
	let localPeer = this.peerConnections[query.a.originId.toString()];
	if(!localPeer){
		//first connection
		if (isNode) {
			localPeer = new SimplePeer({initiator: false, trickle: false, wrtc: wrtc});
		} else {
			localPeer = new SimplePeer({initiator: false, trickle: false, });
		}
		this.peerConnections[query.a.originId.toString()] = localPeer;
		localPeer.on("signal", (signal) => {
			const msg = {y: "q", q: "connection", a: {
				targetId: query.a.originId.toString(),
				originId: this.nodeId.toString("hex"),
				signal: JSON.stringify(signal)
			}};
			peer.send(Bencode.encode(msg));
		});
		localPeer.on("connect", () => {
			console.log("a breez!");
		});
		localPeer.on("data", (data) => {
			console.log("New data from remote: " + data);
		});
		localPeer.on("close", () => {
			console.log("connection closed");
		});
		localPeer.on("stream", (stream) => {
			if(self.streamHandler !== undefined){
				self.streamHandler(stream);
			}
		});
		localPeer.on("track", (track, stream) => {
			if(self.trackHandler !== undefined){
				self.trackHandler(track, stream);
			}
		});
	}
	localPeer.signal(JSON.parse(query.a.signal));
};

/**
 * Reaction to a 'connection' query
 * @param query Message object with which the query event is emitted
 * @param peer Remote peer sending the query
 * @private
 */
WebtorrentDhtCustom.prototype._onconnection = function(query, peer){
	console.log("connection");
	const senderPeer = this._rpc.socket.socket._peer_connections[peer.address + ":" + peer.port];
	console.log(new TextDecoder().decode(query.a.signal));
	this._processConnection(query, senderPeer);
};

/**
 * Gets a query object and prints it to the console.
 * @param query
 * @private
 */
WebtorrentDhtCustom.prototype._onMessage = function(query){
	//Keys of msg dict can be transformed via .toString()
	console.log("Received general message. Content: ");
	console.log(query.a.toString(), query.q.toString(), query.y.toString());
};

/**
 * Check if there is a sessionKey saved for the sending peer. <br>
 * If so decrypt the message with AES and process it.
 * If not handle it as a AES session initiation message and decrypt it with the RSA private key.
 * @param {Object} query The query containing the encrypted message.
 * @param {Object} peer Object holding address and port of the local end of the connection to the sender.
 * @private
 */
WebtorrentDhtCustom.prototype._onEncryptedMsg = function(query, peer){
	//Check if a session key for remote peer is present
	//If so decrypt with that and act according to content
	//If not, decrypt with privateKey and store AES key. If it is no AES key ignore the message.

	console.log("got encrypted_msg");
	let self = this;
	let remoteNodeId = self._rpc.socket.socket._peer_connections[peer.address + ":" + peer.port].id;
	if(self.sessionKeys[remoteNodeId] === undefined){
		//no session - suppose this is a encrypted session key
		self._decryptSessionKey(query).then((msg) => {
			console.log(msg);
			return self._saveSessionKey(remoteNodeId, msg.key);
		}).then(() => {
			console.log(self.sessionKeys);
		});
	} else {
		//session exists - suppose this is signaling data
		console.log("session already exists");
		console.log(query);
		self._decryptAESMessage(query, self.sessionKeys[remoteNodeId]).then((decryptedMsg) => {
			self._onquery(decryptedMsg, peer);
		});
	}
};

/**
 * Helper method to decrypt a decrypted message using RSA.
 * @param {Object} query The message to be decrypted.
 * @returns {Promise} Promise resolving to the decrypted message object.
 * @private
 */
WebtorrentDhtCustom.prototype._decryptSessionKey = function(query){
	let data = new Uint8Array(Object.values(query.a));
	return global.crypto.subtle.decrypt({name: "RSA-OAEP"}, this.keyPair.privateKey, data.buffer).then((decrypt) => {
		return JSON.parse(new TextDecoder().decode(decrypt));
	}).catch((error) => {
		console.error(error);
	});
};

/**
 * Converts a jwk AES key to a CryptoKey and saves it to the object.
 * @param {String} remoteNodeId nodeId of the remote end.
 * @param {Object} key JWK representation of the key to be saved.
 * @returns {Promise} Promise which saves the imported key to the object when finished.
 * @private
 */
WebtorrentDhtCustom.prototype._saveSessionKey = function(remoteNodeId, key){
	let self = this;
	let alg = {name: "AES-GCM"};

	return global.crypto.subtle.importKey("jwk", key, alg, true, ["encrypt", "decrypt"]).then((key) => {
		self.sessionKeys[remoteNodeId] = key;
	});
};

/**
 *
 * @param {Object} query Object representation of a Uint8Array
 * @param {Object} sessionKey CryptoKey
 * @returns {Promise}
 * @private
 */
WebtorrentDhtCustom.prototype._decryptAESMessage = function(query, sessionKey){
	let self = this;
	let iv = new Uint8Array(Object.values(query.a.iv));
	let alg = {name: "AES-GCM", iv: iv};
	let data = new Uint8Array(Object.values(query.a.msg));

	return global.crypto.subtle.decrypt(alg, sessionKey, data.buffer).then((decrypted) => {
		return JSON.parse(new TextDecoder().decode(decrypted));
	}).catch((err) => {
		console.error(err);
	});
};

WebtorrentDhtCustom.prototype.on("node", (node) => {
	console.log("Node event caught:");
	console.log(JSON.stringify(node));
});
