"use strict";
const DHT = require("../main");
const BUFFER = require("buffer").Buffer;


const CLIENT_OPTIONS = {
	nodes: [{
		host: "127.0.0.1",
		port: 16881
	}],
	simple_peer_opts: {
		config: {
			iceServers: []
		}
	},
	isServer: false,
	streamHandler: (stream) => {
		let video = document.createElement("video");
		video.srcObject = stream;
		document.body.appendChild(video);
		video.play();
	},
};

global.dht = undefined;

document.getElementById("showConnections").onclick = showConnections;
document.getElementById("createIDButton").onclick = createSession;
document.getElementById("connectButton").onclick = connectSession;
document.getElementById("activateMedia").onclick = activateMedia;
document.getElementById("addMedia").onclick = addMedia;
//TODO Output is done via console atm. Find a way to use events or sth similar so the node status div is changed.
function main(){
	global.dht = new DHT(CLIENT_OPTIONS);
}

function showConnections(){
	let dict = global.dht.peerConnections;
	for(let key in dict){
		document.getElementById("status").innerHTML += dict[key]["id"] + "<br>";
	}
}

function createSession(){
	const deckID = document.getElementById("deckID").value;
	global.dht.cioCreator(deckID).then((value) => {
		document.getElementById("status").innerHTML += "Hash: " + value + "<br>";
	});
}

function connectSession(){
	const sessionHash = document.getElementById("connectID").value;
	global.dht.retrieveCio(sessionHash).then((sessionDesc) => {
		global.dht.initConnection(sessionDesc, {initiator: true, trickle: false}).then((peer) => {
			document.getElementById("status").innerHTML += "Peer: " + peer.localAddress + "<br>";
		});
	});
}

function activateMedia(){
	let video = document.querySelector("video");
	navigator.mediaDevices.getUserMedia({audio: true, video: true}).then((stream) => {
		video.srcObject = stream;
	});
}

function addMedia(){
	let video = document.querySelector("video").srcObject;
	let targetID = document.getElementById("targetId").value;
	global.dht.peerConnections[targetID].addStream(video);
}

main();
